package renamer;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import renamer.customControls.NumberTextField;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static java.nio.file.Files.isRegularFile;
import static renamer.util.AppConstants.HOME_DIR;
import static renamer.util.AppConstants.LABELS;
import static renamer.util.AppUtils.addStyleClass;
import static renamer.util.AppUtils.renameFile;

public class Controller implements Initializable {

    private static final Logger logger = LogManager.getLogger();

    private static File initialDirectory;

    @FXML
    private VBox primaryPane;
    @FXML
    private MenuItem filesChooser;
    @FXML
    private MenuItem folderChooser;
    @FXML
    private ListView<Path> filesPathsList;
    @FXML
    private TextField newFileName;
    @FXML
    private NumberTextField startNumberStr;
    @FXML
    private Button startRenameBtn;
    @FXML
    private ProgressBar progressBar;

    private ObservableList<Path> filesPaths;

    private int startNumber;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initialDirectory = Paths.get(HOME_DIR).toFile();
        filesPaths = filesPathsList.getItems();

        filesChooser.setOnAction(this::chooseFiles);
        folderChooser.setOnAction(this::chooseFolder);
        startRenameBtn.setOnAction(this::startRename);
        newFileName.setOnKeyTyped(this::textFieldKeyTypedListener);
        startNumberStr.setOnKeyTyped(this::textFieldKeyTypedListener);
        filesPaths.addListener(this::filesPathsChangeListener);

        Platform.runLater(()->{
            startNumberStr.setPrefWidth(startNumberStr.getScene().getWidth()*0.1);
            progressBar.setPrefWidth(progressBar.getScene().getWidth());
        });
    }

    /**
     * Shows files selection dialog.
     */
    private void chooseFiles(ActionEvent event) {
        logger.info("Button '" + filesChooser.getId() + "' has been clicked");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(LABELS.getString("fileChooserTitle"));
        fileChooser.setInitialDirectory(initialDirectory);

        List<File> files = fileChooser.showOpenMultipleDialog(primaryPane.getScene().getWindow());
        if (files != null) {
            logger.info("Files have been selected");

            filesPaths.clear();
            filesPaths.addAll(files.stream().map(File::toPath).collect(Collectors.toList()));
            initialDirectory = files.get(files.size() - 1).getParentFile().getAbsoluteFile();
        } else {
            logger.info("Files haven't been selected");
        }
    }

    /**
     * Shows a folder chooser dialog.
     * Chooses all files in a selected folder.
     */
    private void chooseFolder(ActionEvent event) {
        logger.info("Button '" + folderChooser.getId() + "' has been clicked");

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle(LABELS.getString("folderChooserTitle"));
        directoryChooser.setInitialDirectory(initialDirectory);

        File directory = directoryChooser.showDialog(primaryPane.getScene().getWindow());
        if (directory != null) {
            logger.info("Folder " + directory.getAbsolutePath() + " has been selected");

            try {
                filesPaths.clear();
                Files.walk(directory.toPath()).forEach(path -> {
                    if (isRegularFile(path)) {
                        filesPaths.add(path);
                    }
                });
                initialDirectory = directory.getParentFile().getAbsoluteFile();
            } catch (IOException e) {
                logger.error("Cannot read files from directory {" + directory.getAbsolutePath() + "}");
            }
        } else {
            logger.info("Folder hasn't been selected");
        }
    }

    /**
     * Starts rename files in background process.
     */
    private void startRename(ActionEvent event) {
        Thread backgroundThread = new Thread(() -> {
            logger.info("Start rename files...");

            if (checkRequiredFields()) {
                progressBar.setVisible(true);

                double progressStep = 1.0 / filesPaths.size();
                int counter = startNumber;
                ObservableList<Path> newFilesPaths = FXCollections.observableArrayList();
                for (Path filesPath : filesPaths) {
                    newFilesPaths.add(renameFile(filesPath.toFile(), newFileName.getText(), counter));
                    counter++;
                    Platform.runLater(() -> progressBar.setProgress(progressBar.getProgress() + progressStep));
                }

                progressBar.setVisible(false);
                progressBar.setProgress(0.0);
                Platform.runLater(() -> filesPaths.setAll(newFilesPaths));


                logger.info("Files have been renamed");
            }
        });
        backgroundThread.setDaemon(true);
        backgroundThread.start();
    }

    /**
     * Checks whether all data have been inputted correctly.
     *
     * @return <code>true</code> if all data have been inputted correctly.
     */
    private boolean checkRequiredFields() {

        boolean isFilledRight = true;

        if (filesPaths.isEmpty()) {
            logger.info("Files haven't been selected");
            addStyleClass(filesPathsList, "error");
            isFilledRight = false;
        }

        newFileName.setText(newFileName.getText().trim());
        if (newFileName.getText().isEmpty()) {
            logger.info("File name has not been inputted");
            addStyleClass(newFileName, "error");
            isFilledRight = false;
        }

        if (startNumberStr.getText().isEmpty()) {
            logger.info("Start number has not been inputted");
            addStyleClass(startNumberStr, "error");
            isFilledRight = false;
        } else {
            try {
                startNumber = Integer.parseInt(startNumberStr.getText());
                if (filesPaths.size() > (Integer.MAX_VALUE - startNumber)) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                logger.error("Start number = " + startNumberStr.getText() + " is too big");
                addStyleClass(startNumberStr, "error");
                isFilledRight = false;
            }
        }

        return isFilledRight;
    }

    /**
     * Called after a key was pressed in a text field.
     *
     * @param keyEvent is an event which indicates that a keystroke occurred.
     */
    private void textFieldKeyTypedListener(KeyEvent keyEvent) {
        TextField textField = (TextField) keyEvent.getSource();
        textField.getStyleClass().remove("error");
    }

    /**
     * Called after a change has been made to an ObservableList.
     *
     * @param c an object representing the change that was done
     */
    private void filesPathsChangeListener(ListChangeListener.Change<? extends Path> c) {
        if (!c.getList().isEmpty()) {
            filesPathsList.getStyleClass().remove("error");
        }
    }
}
