package renamer.util;

import javafx.scene.control.Control;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static renamer.util.AppConstants.VIEWS_DIR;

public final class AppUtils {

    private static final Logger logger = LogManager.getLogger();

    private AppUtils() {
    }

    /**
     * Returns url of passed view name.
     *
     * @param viewName is view name without extension.
     * @return url of passed view name.
     */
    public static URL getViewUrl(String viewName) {
        return AppUtils.class.getClassLoader().getResource(VIEWS_DIR + viewName + ".fxml");
    }

    /**
     * Appends the specified style class to the <code>node</code>. If a style class has already been added
     * or contains specified style, then the style class would not be added.
     *
     * @param node       to which will be added to the style class.
     * @param styleClass is a name of the style class, which will be added to the <code>node</code>.
     */
    public static void addStyleClass(Control node, String styleClass) {
        if (!node.getStyleClass().contains(styleClass)) {
            node.getStyleClass().add(styleClass);
        }
    }

    /**
     * Renames a file.
     *
     * @param file    is a file, which will be renamed.
     * @param newName is a new filename.
     * @return new file path if the file has been renamed successfully or old file path otherwise.
     */
    public static Path renameFile(File file, String newName, int counter) {

        Path filePath;
        String oldFileName = file.getName();
        String fileExtension = getFileExtension(oldFileName);
        String newFileName = newName + "_" + counter + "." + fileExtension;

        try {
            Path renamedFilePath = Paths.get(file.getParent() + File.separator + newFileName);
            filePath = Files.move(file.toPath(), renamedFilePath);
            logger.info("File " + file.getAbsolutePath() + " has been renamed");
        } catch (IOException e) {
            filePath = file.toPath();
            logger.error("Could not rename file " + file.getAbsolutePath());
        }

        return filePath;
    }

    /**
     * Returns the file extension for a given file name or empty string if file does not have extension.
     *
     * @param fileName it's a name of passed file or full path to the file.
     * @return the file extension for a given file name or empty string if file does not have extension.
     */
    private static String getFileExtension(String fileName) {
        String fileExtension;
        if (fileName.contains(".")) {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
            logger.info("File extension is '*." + fileExtension);
        } else {
            fileExtension = "";
            logger.info("File does not have extension or it is a folder");
        }
        return fileExtension;
    }
}
