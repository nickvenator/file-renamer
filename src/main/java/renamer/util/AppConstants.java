package renamer.util;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public final class AppConstants {

    public static final String HOME_DIR = System.getProperty("user.home");
    public static final ResourceBundle LABELS = ResourceBundle.getBundle("labels");
    public static final Rectangle2D SCREEN_BOUNDS = Screen.getPrimary().getVisualBounds();
    public static final long DEFAULT_FONT_SIZE = Math.round(SCREEN_BOUNDS.getMaxX() / 114.285714286);
    public static final String VIEWS_DIR = "view/";
    public static final String DEFAULT_FONT_FAMILY = getDefaultFontFamilyName();

    private AppConstants() {
    }

    /**
     * Returns one of the default font family name or generic-family name "sans-serif"
     * if default fonts have not been found.
     *
     * @return one of the default font family name.
     */
    private static String getDefaultFontFamilyName() {
        String[] defaultFontFamilyNames = {"Verdana", "Ubuntu", "Geneva CY"};
        String[] availableFontFamilyNames = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        List<String> fontFamilyNames = Arrays.asList(availableFontFamilyNames);

        for (String fontFamilyName : defaultFontFamilyNames) {
            if (fontFamilyNames.contains(fontFamilyName)) {
                return fontFamilyName;
            }
        }
        return "sans-serif";
    }
}
