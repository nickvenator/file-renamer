package renamer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import static renamer.util.AppConstants.*;
import static renamer.util.AppUtils.getViewUrl;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent rootNode = FXMLLoader.load(getViewUrl("primaryView"), LABELS);

        Scene scene = new Scene(rootNode);
        scene.getStylesheets().add("css/style.css");
        scene.getRoot().setStyle("-fx-font:" + DEFAULT_FONT_SIZE + " " + DEFAULT_FONT_FAMILY + ";");

        primaryStage.setTitle(LABELS.getString("fileRenamer"));
        primaryStage.setWidth(SCREEN_BOUNDS.getMaxX() * 0.4);
        primaryStage.setHeight(SCREEN_BOUNDS.getMaxY() * 0.3);
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
